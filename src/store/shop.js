import data from './data.json'
export default {
    state: {
        shopElements : data,
        index : 0
    },
    mutations: {
        setShopElements(state,elements){
            state.shopElements = elements
        },
        nextElement(state){
            state.index++
        },
        previousElement(state){
            if( state.index > 0){
                state.index--

            }
        },
        moveElementToBucket(state){
            state.shopElements.splice(state.index,1)
        },
        getElementFromBucket(state, element){
            state.shopElements.push(element)
        }
    },
    actions: {},
    getters: {
        firstElement(state){
            return !state.index
        },
        lastElement(state){
            return state.index==state.shopElements.length - 1
        },
        shopElements(state){
            return state.shopElements
        },
        index(state){
            return state.index
        },
        emptyShopList(state){
            return !state.shopElements.length
        }
    },
}
