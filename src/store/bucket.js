export default {
    state: {
        bucketElements: [],
        selectedElementsIndexes: []
    },
    mutations: {
        selectElement(state,index){
            state.selectedElementsIndexes.push(index)
        },
        unselectElement(state,index){
            state.selectedElementsIndexes.splice(index,1)
        },
        getElementFromShop(state,element){
            state.bucketElements.push(element)
        },
        moveElementToShop(state,index){
            state.bucketElements.splice(index,1)
        },
        selectEveryElement(state){
            state.selectedElementsIndexes = []
            state.bucketElements.forEach((elem,index) => {
                state.selectedElementsIndexes.push(index)
            })
        },
        unselectEveryElement(state){
            state.selectedElementsIndexes = []
        }

    },
    actions: {
        moveElementsToShop({commit,state}){
            state.selectedElementsIndexes.sort((a,b)=>a-b)
            for (let i = state.selectedElementsIndexes.length-1; i > -1; i--) {
                commit('getElementFromBucket', state.bucketElements[state.selectedElementsIndexes[i]])
                commit('moveElementToShop',state.selectedElementsIndexes[i])
            }
            commit('unselectEveryElement')
        },
        clickCheckbox({commit,state},index){
            let indexOfIndex = state.selectedElementsIndexes.findIndex(elem => elem==index)
            if(indexOfIndex==-1){
                commit('selectElement',index)
            }else{
                commit('unselectElement',indexOfIndex)
            }
        }
    },
    getters: {
        nothingSelected(state){
            return !state.selectedElementsIndexes.length
        },
        everythingSelected(state){
            return state.selectedElementsIndexes.length == state.bucketElements.length
        },
        bucketElements(state){
            return state.bucketElements
        },
        selectedElementsIndexes(state){
            return state.selectedElementsIndexes
        },
        emptyBucketList(state){
            return !state.bucketElements.length
        }
    },
}
