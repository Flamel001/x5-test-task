import Vue from 'vue'
import Vuex from 'vuex'
import bucket from './bucket'
import shop from './shop'

Vue.use(Vuex)

export default new Vuex.Store({

    modules: {
        bucket, shop
    }
    
})
